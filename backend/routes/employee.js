const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.post('/add', (request, response) => {
  const { empid, name, salary, age } = request.body

  const query = `
    INSERT INTO Emp
      (empid, name, salary, age)
    VALUES
      ('${empid}', '${name}', '${salary}', '${age}')
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.get('/get', (request, response) => {
  const query = `
    SELECT empid, name, salary, age 
    FROM Emp
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.put('/:empid', (request, response) => {
  const { empid } = request.params
  const { name, salary, age } = request.body

  const query = `
    UPDATE Emp
    SET
      name = '${name}', 
      salary = '${salary}', 
      age = '${age}' 
    WHERE empid = ${empid}`
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.delete('/:empid', (request, response) => {
  const { empid } = request.params

  const query = `
    DELETE FROM Emp
    WHERE
    empid = '${empid}'
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

module.exports = router
