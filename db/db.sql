CREATE TABLE Emp (
    empid INTEGER PRIMARY KEY, 
    name VARCHAR(100), 
    salary FLOAT, 
    age INTEGER
);